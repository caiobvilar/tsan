//BASE.CPP//
#include "base.hpp"
//#include <ctime>
//#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <fstream>
#include <vector>
//#include <iosfwd>
using namespace std;

bool fileExists(const char *filename)
{
    stats file;
    return (stat(filename, &file) == 0);
}


int Packet::tshdproc2(Packet *p,char *filename)
{
    ifstream file;
    unsigned char sync = 0;
    file.open(filename);
    unsigned char buffer[PACKSIZE];
    while(!file.eof())
    {
        file.read((char*)&buffer,PACKSIZE);
        if(buffer[0] !=0x47)
        {
            while(sync != 0x47)
            {
                file.read((char*)&sync,1);

            }
            file.seekg(ios::cur -1);
            continue;
        }
        p->header.TEI = ((buffer[1] & 0x80) != 0);
        p->header.PUSI = ((buffer[1] & 0x40) != 0x00);
        p->header.TP = ((buffer[1] & 0x20) != 0x00);
        unsigned short int aux = ((buffer[1] & 0x1f /*0x1f 11111*/) << 8) | buffer[2];
        if((aux ^ 0x1fff) == 0x0000)
        {   
            cout << "Null Packet found, Packet ignored." << endl;
            //return -1;
        }
        p->header.PID = aux;
        p->header.SCRC = (buffer[3] & 0xc0) >> 6;
        p->header.ADFC = (buffer[3] & 0x3) >> 4;
        p->header.CC = (buffer[3] & 0xf);
        /*
        file << "///////////PACKET: [" << p->pnum << "]///////////" << endl;
        file << "SYNC: " << bitset<8> (p->header.SYNC) << endl;
        file << "TEI: " << bitset<1> (p->header.TEI) << endl;
        file << "PUSI: " << bitset<1>(p->header.PUSI) << endl;
        file << "TP: " << bitset<1>(p->header.TP) << endl;
        file << "PID: " << p->header.PID << "["<< bitset<13>(p->header.PID) << "]" << p->header.PID <<endl;
        file << "SCRC: " << bitset<2>(p->header.SCRC) << endl;
        file << "CC: " << bitset<8> (p->header.CC) << endl;
        file << "UNSYNC: " << p->header.UNSYNC << endl;
        file << "Ignored byte: " << ignored << endl;
        file << "/////////////////////////////////////////////////" << endl;
        */
        //cout << "///////////PACKET: [" << p->pnum << "]///////////" << endl;
        cout << "SYNC: " << hex << (sync)<< dec << endl;
        cout << "TEI: " << bitset<1> (p->header.TEI) << endl;
        cout << "PUSI: " << bitset<1>(p->header.PUSI) << endl;
        cout << "TP: " << bitset<1>(p->header.TP) << endl;
        cout << "PID: " << p->header.PID << "["<< bitset<13>(p->header.PID) << "]" << p->header.PID <<endl;
        cout << "SCRC: " << bitset<2>(p->header.SCRC) << endl;
        cout << "CC: " << bitset<8> (p->header.CC) << endl;
        cout << "UNSYNC: " << p->header.UNSYNC << endl;
        cout << "BUFFER[1]:" << bitset<8>(buffer[1]) << endl <<"BUFFER[2]:        " << bitset<8>(buffer[2]) << endl <<"          "<< bitset<16>(aux) << endl;
        //cout << "Ignored byte: " << ignored << endl;
        if(p->header.PID == 0)
        {
            std::vector<unsigned char> table[1024];


        }
        cout << "/////////////////////////////////////////////////" << endl;
    }
    file.close();
    return 0;
}


int crc_32_check(unsigned char *buffer,int startbyte, int endbyte)
{   
    bit2bitAcss crcant[CRCSIZE];
    crcant[0].pack_byte = 0xf;
    crcant[1].pack_byte = 0xf;              //initial Z state = 0xffff
    crcant[2].pack_byte = 0xf;
    crcant[3].pack_byte = 0xf;
    bit2bitAcss crc[CRCSIZE];               //final Z state = CRC Generator Polynomial
    bit2bitAcss buf;
    int bytecount = startbyte;
    int shiftcount = 0;
    int checkcount = 0;
    int transfcount = 0;
    /////////////////////////////////////////////////////////////////////////////////////////////
    //crcant[3].bt.bit7 == Z31
    //
    //Z(i) = Z(31) + Z(i-1) + I(i)
    //Z(i) -> ith polynomial position
    //Z(31) ->31th bit of the initial Z polynom
    //I(i) -> ith bit of the buffer
    //ignored Z`s = 3,6,9,13,14,15,16,18,19,20,21,24,25,26,27,28,29,30,31
    /////////////////////////////////////////////////////////////////////////////////////////////
    buf.pack_byte = buffer[bytecount];
    while(bytecount <= endbyte)
    {
        if(shiftcount  == 8)
        {
            shiftcount = 0;
        }
        while(shiftcount < 8)
        {
            crc[0].bt.bit0 = crcant[3].bt.bit7 + buf.bt.bit7;                         //Z0 -> buffer bit 0
            crc[0].bt.bit1 = crcant[3].bt.bit7 + crcant[0].bt.bit0 + buf.bt.bit7;     //Z1 -> buffer bit 1
            crc[0].bt.bit2 = crcant[3].bt.bit7 + crcant[0].bt.bit1 + buf.bt.bit7;     //Z2 -> buffer bit 2
            crc[0].bt.bit3 = crcant[0].bt.bit2;                                       //Z3 -> buffer bit 3
            crc[0].bt.bit4 = crcant[3].bt.bit7 + crcant[0].bt.bit3 + buf.bt.bit7;     //Z4 -> buffer bit 4
            crc[0].bt.bit5 = crcant[3].bt.bit7 + crcant[0].bt.bit4 + buf.bt.bit7;     //Z5 -> buffer bit 5
            crc[0].bt.bit6 = crcant[0].bt.bit5;                                       //Z6 -> buffer bit 6
            crc[0].bt.bit7 = crcant[3].bt.bit7 + crcant[0].bt.bit6 + buf.bt.bit7;     //Z7 -> buffer bit 7
            crc[1].bt.bit0 = crcant[3].bt.bit7 + crcant[0].bt.bit7 + buf.bt.bit7;     //Z8 -> buffer bit 8
            crc[1].bt.bit1 = crcant[1].bt.bit0;                                       //Z9 -> buffer bit 9
            crc[1].bt.bit2 = crcant[3].bt.bit7 + crcant[1].bt.bit1 + buf.bt.bit7;     //Z10 -> buffer bit 10
            crc[1].bt.bit3 = crcant[3].bt.bit7 + crcant[1].bt.bit2 + buf.bt.bit7;     //Z11 -> buffer bit 11
            crc[1].bt.bit4 = crcant[3].bt.bit7 + crcant[1].bt.bit3 + buf.bt.bit7;     //Z12 -> buffer bit 12
            crc[1].bt.bit5 = crcant[1].bt.bit4;                                       //Z13 = Z12 -> buffer bit 13
            crc[1].bt.bit6 = crcant[1].bt.bit4;                                       //Z14 = Z12 -> buffer bit 14
            crc[1].bt.bit7 = crcant[1].bt.bit4;                                       //Z15 = Z12 -> buffer bit 15
            crc[2].bt.bit0 = crcant[3].bt.bit7 + crcant[1].bt.bit7 + buf.bt.bit7;     //Z16 -> buffer bit 16
            crc[2].bt.bit1 = crcant[2].bt.bit0;                                       //Z17 = Z16 -> buffer bit 17
            crc[2].bt.bit2 = crcant[2].bt.bit0;                                       //Z18 = Z16 -> buffer bit 18
            crc[2].bt.bit3 = crcant[2].bt.bit0;                                       //Z19 = Z16 -> buffer bit 19
            crc[2].bt.bit4 = crcant[2].bt.bit0;                                       //Z20 = Z16 -> buffer bit 20
            crc[2].bt.bit5 = crcant[2].bt.bit0;                                       //Z21 = Z16 -> buffer bit 21
            crc[2].bt.bit6 = crcant[3].bt.bit7 + crcant[2].bt.bit0 + buf.bt.bit7;     //Z22 -> buffer bit 22
            crc[2].bt.bit7 = crcant[3].bt.bit7 + crcant[2].bt.bit6 + buf.bt.bit7;     //Z23 -> buffer bit 23
            crc[3].bt.bit0 = crcant[2].bt.bit7;                                       //Z24 = Z23 -> buffer bit 24
            crc[3].bt.bit1 = crcant[2].bt.bit7;                                       //Z25 = Z23-> buffer bit 25
            crc[3].bt.bit2 = crcant[3].bt.bit7 + crcant[3].bt.bit1 + buf.bt.bit7;     //Z26 -> buffer bit 26
            crc[3].bt.bit3 = crcant[3].bt.bit2;                                       //Z27 = Z26 -> buffer bit 27
            crc[3].bt.bit4 = crcant[3].bt.bit2;                                       //Z28 = Z26-> buffer bit 28
            crc[3].bt.bit5 = crcant[3].bt.bit2;                                       //Z29 = Z26-> buffer bit 29
            crc[3].bt.bit6 = crcant[3].bt.bit2;                                       //Z30 = Z26-> buffer bit 30
            crc[3].bt.bit7 = crcant[3].bt.bit7 + crcant[3].bt.bit6 + buf.bt.bit7;     //Z31 -> buffer bit 31
            buf.pack_byte = buffer[bytecount] << (shiftcount + 1);
            if(transfcount == 3)
            {
                transfcount = 0;
            }
            while(transfcount <= 3)
            {
                crcant[transfcount].pack_byte = crc[transfcount].pack_byte;
                checkcount++;
            }
            shiftcount++;
        }
    }
    while(checkcount <= 3)
    {
        if(crcant[checkcount].pack_byte != 0x00)
        {
            cout << "CRC doesn`t check, Packet not ok!." << endl;
            checkcount++;
        }
        bytecount++;
    }
    return 0;
    
}




int adfproc(Packet *p,unsigned char *buffer) //bytes [5-35]
{
    bit2bitAcss adf[5];  // defined to 5 octets, which is the biggest bytecount in this field
    if((p->adf.ADFL = buffer[5]) == 0x0000)
    {
        cerr << ("Stuffing bytes, Packet ignored.") << endl;;
    }
    adf[0].pack_byte = buffer[6];               //
    p->adf.DSI = adf[0].bt.bit0;                //
    p->adf.RAI = adf[0].bt.bit1;                //
    p->adf.ESPI = adf[0].bt.bit2;               //
    p->adf.PCRF = adf[0].bt.bit3;               //
    p->adf.OPCRF = adf[0].bt.bit4;              //
    p->adf.SPF = adf[0].bt.bit5;                //
    p->adf.TPDF = adf[0].bt.bit6;               //
    p->adf.AFEF = adf[0].bt.bit7;               //
    adf[0].pack_byte = buffer[7];               //
    adf[1].pack_byte = buffer[8];               //
    adf[2].pack_byte = buffer[9];               //
    adf[3].pack_byte = buffer[10];              //
    adf[4].pack_byte = buffer[11];              //
    p->adf.PCRB = adf[0].pack_byte;             // Inserts first 8 bits into the PCRB field
    p->adf.PCRB <<= 8;                          // Shifts 8 positions to accomodate more 8 bits
    p->adf.PCRB |= adf[1].pack_byte;            // Inserts second octet
    p->adf.PCRB <<= 8;                          // Shifts 8 more positions to more 8 bits
    p->adf.PCRB |= adf[2].pack_byte;            // Inserts third octet
    p->adf.PCRB = p->adf.PCRB << 8;             // Shifts 8 more positions
    p->adf.PCRB |= adf[3].pack_byte;            // Inserts fourth octet
    p->adf.PCRB  = p->adf.PCRB << 8;            // Shifts 8 more positions
    p->adf.PCRB |= adf[4].pack_byte;            // Inserts fifth and last octet
    adf[0].pack_byte = buffer[12] << 6;         // Inserts q octet shifted 6 times
    p->adf.PCRE = adf[0].pack_byte;             // Allocates 2 bits from the 12th byte of the buffer
    p->adf.PCRE <<= 1;                          // Shifts 1/7 bit
    adf[0].pack_byte = buffer[13];              // Adds 1 byte to the working buffer
    p->adf.PCRE |= adf[0].bt.bit0;              // Add 1 bit of 1/7 bits
    p->adf.PCRE <<= 1;                          // Shifts 2/7 bit
    p->adf.PCRE |= adf[0].bt.bit1;              // Adds 1 bit of 2/7 bits
    p->adf.PCRE <<= 1;                          // Shitfs 3/7 bit
    p->adf.PCRE |= adf[0].bt.bit2;              // Adds 1 bit of 3/7 bits
    p->adf.PCRE <<= 1;                          // Shifts 4/7 bit
    p->adf.PCRE |= adf[0].bt.bit3;              // Adds 1 bit of 4/7 bits
    p->adf.PCRE <<= 1;                          // Shifts 5/7 bit
    p->adf.PCRE |= adf[0].bt.bit4;              // Adds 1 bit of 5/7 bits
    p->adf.PCRE <<= 1;                          // Shifts 6/7 bit
    p->adf.PCRE |= adf[0].bt.bit5;              // Adds 1 bit of 6/7 bits
    p->adf.PCRE <<= 1;                          // Shifts 7/7 bits
    p->adf.PCRE |= adf[0].bt.bit6;              // Adds 1 bit of 7/7 bits
    p->adf.OPCRB = adf[0].bt.bit7;
    p->adf.OPCRB <<= 8;
    adf[0].pack_byte = buffer[14];
    adf[1].pack_byte = buffer[15];
    adf[2].pack_byte = buffer[16];
    adf[3].pack_byte = buffer[17];
    adf[4].pack_byte = buffer[18];
    p->adf.OPCRB |= adf[0].pack_byte;
    p->adf.OPCRB <<= 8;
    p->adf.OPCRB |= adf[1].pack_byte;
    p->adf.OPCRB <<= 8;
    p->adf.OPCRB |= adf[2].pack_byte;
    p->adf.OPCRB <<= 8;
    p->adf.OPCRB |= adf[3].pack_byte;
    p->adf.OPCRB <<= 8;
    p->adf.OPCRB |= adf[4].pack_byte;
//////////////////////////////////////////////////
    adf[0].pack_byte = buffer[19] << 6;         // Inserts q octet shifted 6 times
    p->adf.OPCRE = adf[0].pack_byte;            // Allocates 2 bits from the 12th byte of the buffer
    p->adf.OPCRE <<= 1;                         // Shifts 1/7 bit
    adf[0].pack_byte = buffer[20];              // Adds 1 byte to the working buffer
    p->adf.OPCRE |= adf[0].bt.bit0;             // Add 1 bit of 1/7 bits
    p->adf.OPCRE <<= 1;                         // Shifts 2/7 bit
    p->adf.OPCRE |= adf[0].bt.bit1;             // Adds 1 bit of 2/7 bits
    p->adf.OPCRE <<= 1;                         // Shitfs 3/7 bit
    p->adf.OPCRE |= adf[0].bt.bit2;             // Adds 1 bit of 3/7 bits
    p->adf.OPCRE <<= 1;                         // Shifts 4/7 bit
    p->adf.OPCRE |= adf[0].bt.bit3;             // Adds 1 bit of 4/7 bits
    p->adf.OPCRE <<= 1;                         // Shifts 5/7 bit
    p->adf.OPCRE |= adf[0].bt.bit4;             // Adds 1 bit of 5/7 bits
    p->adf.OPCRE <<= 1;                         // Shifts 6/7 bit
    p->adf.OPCRE |= adf[0].bt.bit5;             // Adds 1 bit of 6/7 bits
    p->adf.OPCRE <<= 1;                         // Shifts 7/7 bits
    p->adf.OPCRE |= adf[0].bt.bit6;             // Adds 1 bit of 7/7 bits
    p->adf.SPCD = adf[0].bt.bit7;               // Add 1 bit of 1/7
    adf[0].pack_byte = buffer[21];              // Adds the 21th byte to the working buffer
    p->adf.SPCD <<= 1;                          // Shifts 1/7 bit
    p->adf.SPCD |= adf[0].bt.bit0;              // Adds 1 bit of 2/7
    p->adf.SPCD <<= 1;                          // Shifts 2/7 bit
    p->adf.SPCD |= adf[0].bt.bit1;              // Adds 1 bit of 3/7
    p->adf.SPCD <<= 1;                          // Shifts 3/7 bit
    p->adf.SPCD |= adf[0].bt.bit2;              // Adds 1 bit of 4/7
    p->adf.SPCD <<= 1;                          // Shifts 4/7 bit
    p->adf.SPCD |= adf[0].bt.bit3;              // Adds 1 bit of 5/7
    p->adf.SPCD <<= 1;                          // Shifts 5/7 bit
    p->adf.SPCD |= adf[0].bt.bit4;              // Adds 1 bit of 6/7
    p->adf.SPCD <<= 1;                          // Shifts 6/7 bit
    p->adf.SPCD |= adf[0].bt.bit5;              // Adds 1 bit of 7/7
    p->adf.TPDL = adf[0].bt.bit6;               //
    p->adf.TPDL <<= 1;                          //
    p->adf.TPDL |= adf[0].bt.bit7;              //
    p->adf.TPDL <<= 1;                          //
    adf[0].pack_byte = buffer[22];              //
    p->adf.TPDL |= adf[0].bt.bit0;
    p->adf.TPDL <<= 1;
    p->adf.TPDL |= adf[0].bt.bit1;
    p->adf.TPDL <<= 1;
    p->adf.TPDL |= adf[0].bt.bit2;
    p->adf.TPDL <<= 1;
    p->adf.TPDL |= adf[0].bt.bit3;
    p->adf.TPDL <<= 1;
    p->adf.TPDL |= adf[0].bt.bit4;
    p->adf.TPDL <<= 1;
    p->adf.TPDL |= adf[0].bt.bit5;
    p->adf.PDB = adf[0].bt.bit6;
    p->adf.PDB <<= 1;
    p->adf.PDB |= adf[0].bt.bit7; 
    p->adf.PDB <<= 1;
    adf[0].pack_byte = buffer[23];
    p->adf.PDB |= adf[0].bt.bit0;
    p->adf.PDB <<= 1;
    p->adf.PDB |= adf[0].bt.bit1;
    p->adf.PDB <<= 1;
    p->adf.PDB |= adf[0].bt.bit2;
    p->adf.PDB <<= 1;
    p->adf.PDB |= adf[0].bt.bit3;
    p->adf.PDB <<= 1;
    p->adf.PDB |= adf[0].bt.bit4;
    p->adf.PDB <<= 1;
    p->adf.PDB |= adf[0].bt.bit5;

    //end this function!!//
return 0;
}




int patbuilder(Packet *p, unsigned char *buffer)
{  

    return 0;
}


/*
int pmtbuilder(Packet *p, char *buffer, int fd)
{
    
}
*/



/*
void buildreport(Packet p)
{
    ofstream report;
    
    time_t now = time(NULL);
    tm local = *localtime(&now); 
    char timebuf[32];
    strftime(timebuf, sizeof(timebuf), "%a-%b-%d-%H-%M-%S-%Y.txt",&local);
    report.open(timebuf);
    
    if(fileExists("report.txt") == true)
    {
        report.open("report.txt",ios_base::out | ios_base::app );
    }
    if(report.is_open() == false)
    {
        cerr << "Unable to open report file for writing." << endl;
    }

    report << "///////////////Packet : [" << p.pnum << "]///////////////" << endl;
    report << "SYNC: " << (int)p.header.SYNC << endl;
    report << "TEI: " << (int)p.header.TEI << endl;
    report << "PUSI: " << (int)p.header.PUSI << endl;
    report << "TP: " << (int)p.header.TP << endl;
    report << "PID: " << (int)p.header.PID << endl;
    report << "SCRC: " << (int)p.header.SCRC << endl;
    report << "ADF: " << (int)p.header.ADF << endl;
    report << "PAYLD: " << (int)p.header.PAYLD << endl;
    report << "CC: " << (int)p.header.CC << endl;
    report << "UNSYNC: " << (int)p.header.UNSYNC << endl;
    report << "/////////////////////////////////////////////////////////" << endl;
}
*/
