#include <bitset>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <cstdint>
#include <iomanip>
//If Adaptation Field Control == 0x1
//The N data bytes will be equal to Section Length - 9 bytes and start from the 14th
//byte in the buffer(byte 13)
//
//
//If Adapatation Field Control == 0x3
//The N data Bytes will be equal to Seclen - 9 bytes and start from the 13 + Adaptation Field Length
//
////////////////////////////////////////////////////////////////////////////////////////////////////

#define PACKSIZE 188


using namespace std;

typedef struct header_t                //Packet Header
{

    unsigned char SYNC;     // sync byte if not 71, ignore packet
    unsigned char TEI;      // Transport Error Indicator if 1 > ignore packet
    unsigned char PUSI;     // Payload Unit Start Indicator
    unsigned char TP;       // Transport Priority
    unsigned short int PID; // Packet Identifier
    unsigned char SCRC;     // Scrambling Control
    unsigned char ADFC;     // Adaptation Field Control
    unsigned char CC;       // Continuity Counter;
    unsigned char UNSYNC;   // Unsychronized packets counter

    
}Header;

typedef struct pas_t
{
    unsigned char Table_ID;
    unsigned char Section_Syntax_Indicator;
    unsigned short int Section_Length;
    unsigned short int TS_ID;
    unsigned char Version_Number;
    unsigned char Current_Next_Indicator;
    unsigned char Section_Number;
    unsigned char Last_Section_Indicator;
    unsigned short int Program_Number;
    unsigned short int Program_PID;
    unsigned int CRC;
}PAS;


typedef struct pack_t
{
    Header header;
    PAS pas;
    //unsigned char PMT_Table[1024];
    unsigned char pointer_field;
}Packet;

int main(int argc,char *argv[])
{
    if(argc < 2)
    {
        cout << "Usage ./tsoff </PATH/OF/THE/TSFILE.ts> </PATH/OF/THE/REPORT/FILE.txt>" << endl;
        return -1;
    }
    cout << "argv[1]: " << argv[1] << endl;
    ifstream file;
    unsigned char sync = 0;
    file.open(argv[1]);
    unsigned char buffer[PACKSIZE];
    unsigned char PAT[1024];
    Packet p;
    unsigned int continuity_counter;
    int current_cc;
    int counter = 0;
    while(!file.eof())
    {
        file.read((char*)&buffer,PACKSIZE);
        if(buffer[0] != 0x47)
        {
            while(sync != 0x47)
            {
                file.read((char*)&sync,1);

            }
            file.seekg(ios::cur -1);
            continue;
        }
        unsigned short int aux = ((buffer[1] & 0x1f /*0x1f 11111*/) << 8) | buffer[2];
        if((aux ^ 0x1fff) == 0x0000)
        {   
            counter++;
            continue;
        }
        p.header.TEI = ((buffer[1] & 0x80) != 0);
        p.header.PUSI = ((buffer[1] & 0x40) != 0x00);
        p.header.TP = ((buffer[1] & 0x20) != 0x00);
        p.header.PID = aux;
        p.header.SCRC = (buffer[3] & 0xc0) >> 5;
        p.header.ADFC = (buffer[3] & 0x30) >> 4;
        p.header.CC = (buffer[3] & 0xf);
        
        if((int)p.header.CC == 1)
        {
            current_cc = 0;
        }
        
        /*
        if((int)p.header.CC == 15)
        {
            break;
        }
        */
        if(p.header.PID == 0 && (current_cc <= (int)(p.header.CC)))
        {
                cout << "TS-PACKET: "<< counter << endl;
                cout << "PID:" << p.header.PID<< endl;
                cout << "CC: " << (int)(p.header.CC) << endl;
                cout << "N data: " << ((buffer[6] & 4) | buffer[7]) << endl;
                cout << "Network Program Number : " << (int)(buffer[13] | buffer[14]) << endl
                     << "Network PID: " << (int)((buffer[15] & 0x1f) | buffer[16])  << endl
                     << "Program Number: " << (int)(buffer[17] | buffer[18]) << endl
                     << "Program PID: " << (int)((buffer[19] & 0x1f) | buffer[20]) << endl
                     << "Program Number: " << (int)(buffer[21] | buffer[22]) <<  endl
                     << "Program PID: " << (int)((buffer[23] & 0x1f) | buffer[24]) << endl
                     << "Program Number: " << (int)(buffer[25] | buffer[26]) << endl
                     << "Program PID: " << (int)((buffer[27] & 0x1f) | buffer[28]) << endl;
               current_cc = p.header.CC;
               if(current_cc == 15)
               {
                   break;
               }
            
        }
        counter++;
        continue;
    }
    file.close();    
    return 0;
}



/*
        int i=0;        
        if(p.header.PID == 0 && p.header.SCRC == 1)
        {
            unsigned short int length = (buffer[6] & 4) | buffer[7];

        cout << "///////////////////////////////////////////" << endl;
        cout << "Transport Error Indicator: " << bitset<1>(p.header.TEI) << endl;
        cout << "Paylod Unit Start Indicator: " << bitset<1>(p.header.PUSI) << endl;
        cout << "Transport Priority: " << bitset<1>(p.header.TP) << endl;
        cout << "PID: " << bitset<13>(p.header.PID) <<"["<< hex << p.header.PID << dec <<"]" <<endl;
        cout << "Scrambling Control: " << bitset<2>( p.header.SCRC) << endl;
        cout << "Adaptation Field Control: " << bitset<2>(p.header.ADFC) << endl;
        cout << "Continuity Counter: " << bitset<4>(p.header.CC) << endl;
        cout << "///////////////////////////////////////////" << endl;
        cout << "Pack" << i << " | Seclen: " << length << endl;
        i++;
        }
           cout << "////////////////////////////////////////////////////" << endl 
                 << "// BUFFER[0]  ||  BUFFER[1] || BUFFER[2] || BUFFER[3] || BUFFER[4]//" << endl
                 << "// [" << bitset<8>(buffer[0])<< "] [" << bitset<8>(buffer[1]) << "] [" << bitset<8>(buffer[2]) << "] [" << bitset<8>(buffer[3]) << "]//" << endl
                 << "//  xxxxxxxx   00000000   00000000   00000000   00000000 SYNC: " << bitset<8>(buffer[0]) << endl
                 << "//  00000000   x0000000   00000000   00000000   00000000 TEI: " << bitset<8> (p.header.TEI) << endl
                 << "//  00000000   0x000000   00000000   00000000   00000000 PUSI: " << bitset<8> (p.header.PUSI) << endl
                 << "//  00000000   00x00000   00000000   00000000   00000000 TP: " << bitset<8> (p.header.TP) << endl
                 << "//  00000000   000xxxxx   xxxxxxxx   00000000   00000000 PID: " << bitset <13> (p.header.PID) << endl
                 << "//  00000000   00000000   00000000   xx000000   00000000 SCRC: " << bitset<8> (p.header.SCRC) << endl
                 << "//  00000000   00000000   00000000   00xx0000   00000000 ADFC: " << bitset<8> (p.header.ADFC) << endl
                 << "//  00000000   00000000   00000000   0000xxxx   00000000 CC: " << bitset<8> (p.header.CC) << endl
                 << "//  00000000   00000000   00000000   00000000   xxxxxxxx ADFL: " << (int)buffer[4] << endl
                 << "//  Section Length, according to ADFC 01: " << (int)((buffer[6] & 0xf) | buffer[7]) << endl
                 << "//  Version Number: " << (buffer[9] & 0x1f) << endl
                 << "////////////////////////////////////////////////////" << endl;
        */
