//BASE.H//
#ifndef _BASE_H_
#define _BASE_H_

#include <iostream>
#include <fstream>
using namespace std;
#include <sys/stat.h>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <bitset>
#include <vector>


#define PACKSIZE 188	//bytes in each TS packet
#define PSIZE 1024	//PSI TABLE MAX SIZE IN BYTES
#define CRCSIZE 4       //CRC bit checking in bytes





typedef struct stat stats;


typedef union bit2bitAcess_t{
	struct
	{
		unsigned char bit0 :1;
		unsigned char bit1 :1;
		unsigned char bit2 :1;
		unsigned char bit3 :1;
		unsigned char bit4 :1;
		unsigned char bit5 :1;
		unsigned char bit6 :1;
		unsigned char bit7 :1;
	} __attribute__((__packed__)) bt;
	unsigned char pack_byte;
}bit2bitAcss;

class Header 				//Packet Header
{
public:
	unsigned char SYNC;     // sync byte if not 71, ignore packet
	unsigned char TEI;      // Transport Error Indicator if 1 > ignore packet
	unsigned char PUSI;     // Payload Unit Start Indicator
	unsigned char TP;      // Transport Priority
	unsigned short int PID;          // Packet Identifier
	unsigned char SCRC;     // Scrambling Control
	unsigned char ADFC;     // Adaptation Field Control
	unsigned char CC;       // Continuity Counter;
	unsigned char UNSYNC;             // Unsychronized packets counter

	
};

class Adf					//Adaptation Fied
{
public:
	unsigned char ADFL;		//Adaptation Field Length [8-bits]
	unsigned char DSI;		//Discontinuity Indicator [1-bit]
	unsigned char RAI;		//Random Access Indicator [1-bit]
	unsigned char ESPI;		//Elementary Stream Priority Indicator [1-bit]
	unsigned char PCRF;		//Program Clock Reference Flag [1-bit]
	unsigned char OPCRF;	//Original Program Clock Reference Flag [1-bit]
	unsigned char SPF;		//Splicing Point FLag [1-bit]
	unsigned char TPDF;		//Transport Private Data Flag [1-bit]
	unsigned char AFEF;		//Adaptation Field Extension Flag [1-bit]
	long long int PCRB;  	//Program Clock Reference Base [33-bits]
	unsigned char RESRVD1;	//First Reserved Bitfield [6-bits]
	short int PCRE; 		//Program Clock Reference Extension [9-bits]
	long long int OPCRB;	//Original Program Clock Reference Base [33-bits]
	unsigned char RESRVD2;	//Second Reserved Bitfield [6-bits]
	short int OPCRE;    	//Original Program Clock Reference Extension [9-bits]
	unsigned char SPCD;		//Splice Countdown [8-bits]
	unsigned char TPDL;		//Transport Private Data Length [8-bits]
	unsigned char PDB;		//Private Data Byte [8-bits]
	unsigned char ADFEL;	//Adaptation Field Extension Length [8-bits]
	unsigned char LTWF;		//Legal Time Window Flag [1-bit]
	unsigned char PWRF;		//PieceWise Rate Flag [1-bit]
	unsigned char SSF;		//Seamless Splice Flag [1-bits]
	unsigned char RESRVD3;	//Third Reserved Bitfield [5-bits]
	unsigned char LTWVF;	//Legal Time Window Valid Flag [1-bit]
	short int LTWO; 		//Legal Time Window Offset [15-bits]
	unsigned char RESRVD4;	//Fourth Reserved Bitfield [2-bits]
	long int PWR;     		//PieceWise Rate [22-bits]
	unsigned char SPT;		//Splice Type [4-bits]
	unsigned char DTNA1;	//Decoding Time Stamp Next Access Unit[32...30] [3-bits]
	unsigned char MKRB1;	//Marker Bit1   [1-bit]
	short int DTNA2;    	//Decoding Time Stamp Next Access Unit[29...15] [15-bits]
	unsigned char MKRB2;	//Marker Bit 2  [1-bit]
	short int DTNA3;    	//Decoding Time Stamp Next Access Unit[14...0] [15-bits]
	unsigned char MKRB3;	//Marker Bit 3  [1-bit]
	unsigned char RESRVD5;	//Fifth Reserved Bitfield [8-bits]
	unsigned char STFB;		//Stuffing Byte [8-bits]


};


class Pat
{
public:
	char TID; 			   	//table ID [8-bits]
	char SSI;				//section syntax indicator [1-bit]
    //there is a '0' bit between first reserved bitfield and the section syntax
    //indicator
	char RESERVD1;			//reserved bitfield 1 [2-bits]
	short int SECLEN;		//section length [12-bits]
	short int TSID;			//transport stream ID [16-bits]
	char RESERVD2;			//reserved bitfield 2 [2-bits]
	char VERSN;				//version number [5-bits]
	char CNI;				//current next indicator [1-bit]
	char SECNUM;			//section number [8-bits]
	char LSECNUM;			//last section number [8-bits]
	short int PROGNUM;		//program  number [16-bits]
    char RESERVD3;          //reserved bitfield 3 [3-bits]
	short int NPID;			//network PID [13-bits]
	short int PMPID;		//program map PID [13-bits]
	int CRC_32;				//32-bit CRC check for the PAT [32-bits]


};




class Psi
{
public:
	short int PID;
	unsigned char pidbuffer[PSIZE];
};



class __attribute__((__packed__)) Packet
{
public:
    int pnum;	
	Header header;
	Adf adf;
	Pat pat;
	//Psi psi;
    
	int tshdproc2(Packet *,char *);
	int crc_32_check(unsigned char,int,int);
	int patbuilder(Packet *, unsigned char *, vector<Psi> *);    
	int adfproc(Packet *,unsigned char*);
};

//void buildreport(packet);
//void init

bool fileExists(const char *);

//int pmt_t::pmtbuilder(packet*,unsigned char*);

#endif 
