//#include "base.hpp"
#include <bitset>
//#include <ctime>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstring>


#define PACKSIZE 188


using namespace std;

typedef struct header_t                //Packet Header
{

    unsigned char SYNC;     // sync byte if not 71, ignore packet
    unsigned char TEI;      // Transport Error Indicator if 1 > ignore packet
    unsigned char PUSI;     // Payload Unit Start Indicator
    unsigned char TP;       // Transport Priority
    unsigned short int PID; // Packet Identifier
    unsigned char SCRC;     // Scrambling Control
    unsigned char ADFC;     // Adaptation Field Control
    unsigned char CC;       // Continuity Counter;
    unsigned char UNSYNC;   // Unsychronized packets counter

    
}Header;

typedef struct adaptation_field_t                   //Adaptation Fied
{
    unsigned char ADFL;     //Adaptation Field Length [8-bits]
    unsigned char DSI;      //Discontinuity Indicator [1-bit]
    unsigned char RAI;      //Random Access Indicator [1-bit]
    unsigned char ESPI;     //Elementary Stream Priority Indicator [1-bit]
    unsigned char PCRF;     //Program Clock Reference Flag [1-bit]
    unsigned char OPCRF;    //Original Program Clock Reference Flag [1-bit]
    unsigned char SPF;      //Splicing Point FLag [1-bit]
    unsigned char TPDF;     //Transport Private Data Flag [1-bit]
    unsigned char AFEF;     //Adaptation Field Extension Flag [1-bit]
    long long int PCRB;     //Program Clock Reference Base [33-bits]
    unsigned char RESRVD1;  //First Reserved Bitfield [6-bits]
    short int PCRE;         //Program Clock Reference Extension [9-bits]
    long long int OPCRB;    //Original Program Clock Reference Base [33-bits]
    unsigned char RESRVD2;  //Second Reserved Bitfield [6-bits]
    short int OPCRE;        //Original Program Clock Reference Extension [9-bits]
    unsigned char SPCD;     //Splice Countdown [8-bits]
    unsigned char TPDL;     //Transport Private Data Length [8-bits]
    unsigned char PDB;      //Private Data Byte [8-bits]
    unsigned char ADFEL;    //Adaptation Field Extension Length [8-bits]
    unsigned char LTWF;     //Legal Time Window Flag [1-bit]
    unsigned char PWRF;     //PieceWise Rate Flag [1-bit]
    unsigned char SSF;      //Seamless Splice Flag [1-bits]
    unsigned char RESRVD3;  //Third Reserved Bitfield [5-bits]
    unsigned char LTWVF;    //Legal Time Window Valid Flag [1-bit]
    short int LTWO;         //Legal Time Window Offset [15-bits]
    unsigned char RESRVD4;  //Fourth Reserved Bitfield [2-bits]
    long int PWR;           //PieceWise Rate [22-bits]
    unsigned char SPT;      //Splice Type [4-bits]
    unsigned char DTNA1;    //Decoding Time Stamp Next Access Unit[32...30] [3-bits]
    unsigned char MKRB1;    //Marker Bit1   [1-bit]
    short int DTNA2;        //Decoding Time Stamp Next Access Unit[29...15] [15-bits]
    unsigned char MKRB2;    //Marker Bit 2  [1-bit]
    short int DTNA3;        //Decoding Time Stamp Next Access Unit[14...0] [15-bits]
    unsigned char MKRB3;    //Marker Bit 3  [1-bit]
    unsigned char RESRVD5;  //Fifth Reserved Bitfield [8-bits]
    unsigned char STFB;     //Stuffing Byte [8-bits]


}Adf;


typedef struct pas_t
{
    unsigned char Table_ID;
    unsigned char Section_Syntax_Indicator;
    unsigned short int Section_Length;
    unsigned short int TS_ID;
    unsigned char Version_Number;
    unsigned char Current_Next_Indicator;
    unsigned char Section_Number;
    unsigned char Last_Section_Indicator;
    unsigned short int Program_Number;
    unsigned short int Program_PID;
    unsigned int CRC;
}PAS;


typedef struct pack_t
{
    Header header;
    PAS pas;
    //unsigned char PMT_Table[1024];
    unsigned char pointer_field;
}Packet;

int main(int argc,char *argv[])
{
    if(argc < 2)
    {
        cout << "Usage ./tsoff </PATH/OF/THE/TSFILE.ts> </PATH/OF/THE/REPORT/FILE.txt>" << endl;
        return -1;
    }
    cout << "argv[1]: " << argv[1] << endl;
    ifstream file;
    unsigned char sync = 0;
    file.open(argv[1]);
    unsigned char buffer[PACKSIZE];
    unsigned char PAT[1024];
    Packet p;
    while(!file.eof())
    {
        file.read((char*)&buffer,PACKSIZE);
        if(buffer[0] != 0x47)
        {
            while(sync != 0x47)
            {
                file.read((char*)&sync,1);

            }
            file.seekg(ios::cur -1);
            continue;
        }
        unsigned short int aux = ((buffer[1] & 0x1f /*0x1f 11111*/) << 8) | buffer[2];
        if((aux ^ 0x1fff) == 0x0000)
        {   
            continue;
        }
        p.header.TEI = ((buffer[1] & 0x80) != 0);
        p.header.PUSI = ((buffer[1] & 0x40) != 0x00);
        p.header.TP = ((buffer[1] & 0x20) != 0x00);
        p.header.PID = aux;
        p.header.SCRC = (buffer[3] & 0xc0) >> 6;
        p.header.ADFC = (buffer[3] & 0x3) >> 4;
        p.header.CC = (buffer[3] & 0xf);
        cout << "///////////////////////////////////////////" << endl;
        cout << "Transport Error Indicator: " << bitset<1>(p.header.TEI) << endl;
        cout << "Paylod Unit Start Indicator: " << bitset<1>(p.header.PUSI) << endl;
        cout << "Transport Priority: " << bitset<1>(p.header.TP) << endl;
        cout << "PID: " << bitset<13>(p.header.PID) <<"["<< hex << p.header.PID << dec <<"]" <<endl;
        cout << "Scrambling Control: " << bitset<2>( p.header.SCRC) << endl;
        cout << "Adaptation Field Control: " << bitset<2>(p.header.ADFC) << endl;
        cout << "Continuity Counter: " << bitset<4>(p.header.CC) << endl;
        cout << "///////////////////////////////////////////" << endl;
        /*
        int i = 0;
        
        if(p.header.PID == 0 || )
        {

            p.pointer_field = buffer[34]; 
            memcpy(&[i+(p.pointer_field)].Data,&buffer[42],(p.pointer_field - 184 - )); 
            i += (p.pointer_field - 12);
        }    
        */
    }
    //for( int i = 0;i <=       
    file.close();
    
    return 0;
}




