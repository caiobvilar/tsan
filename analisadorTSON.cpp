#include <netdb.h>
#include <sys/socket.h>
#include <string.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "base.h" //criada pro código


int main(int argc, char *argv[])
{
	
	int port;
	if(argc < 2)
	{
		printf("Too few arguments, you must provide a port as ./tsa <portnumber>.\n");
		return EXIT_FAILURE;
	}
	else
	{
		port = atoi(argv[1]);
	}
	printf("debug argc = %d.\n",argc);

	int listfd,recvfd;
	int sockopt = 1;
	char buffer[PACKSIZE];
	char str[INET_ADDRSTRLEN];
	struct sockaddr_in anl;
	struct sockaddr_in ts;
	socklen_t tslen = sizeof(ts);
	if((listfd = socket(AF_INET, SOCKTYPE,0)) < 0)
	{
		error("Unable to create socket.\n");
	}
	else
	{
		printf("Socket succesfully created.\n");
	}
	if((setsockopt(listfd,SOL_SOCKET,SO_REUSEADDR, &sockopt, sizeof(int))) == -1)
	{
		error("Failed to set socket reuse.\n");	
	}
	else
	{
		printf("Socket set to be reused.\n");
	}
	anl.sin_family = AF_INET;
	anl.sin_port = htons(port);
	anl.sin_addr.s_addr = INADDR_ANY;
	memset(&anl.sin_zero, 0, sizeof(anl.sin_zero));

	if(bind(listfd, (struct sockaddr *)&anl, sizeof(anl)) < 0)
	{
		error("Unable to bind server to designated port.\n");
	}
	else
	{
		printf("Succesfully binded server to designated port:[%d].\n",anl.sin_port);
	}
	while(1)
	{
		if((recvfd = recvfrom(listfd,buffer,PACKSIZE,0,(struct sockaddr *)&ts,&tslen)) < 0)
		{
			error("Failed to receive data.\n");
		}
		else
		{
			printf("Data succesfully received from [%s:%d].\n",inet_ntop(AF_INET,&(ts.sin_addr),str,INET_ADDRSTRLEN),ntohs(ts.sin_port));
			/*if(tsproc(pack,&buffer) < 0)
			{
				printf("TS analyzed.\n");
			}*/
		}
	}
}
